package org.bosh.motion;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bosh.model.AxisDirection;
import org.bosh.model.Coordinates;
import org.bosh.model.RobotPosition;
import org.bosh.model.Rotation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Counter Clockwise rotation by angle of 90 degrees.
 */
@Component("R")
public class RightRotationMotion implements MotionCommand {

	private final Log log = LogFactory.getLog(RightRotationMotion.class);

	@Autowired
	private MotionUtils utils;
	
	@Override
	public String getMotionId() {
		return "right_rotation_motion";
	}

	public final void doMove(RobotPosition robotPosition) {
		AxisDirection direction = robotPosition.getAxisDirection();
		Rotation rotation = utils.getRotationDirection(direction);
		robotPosition.setAxisDirection(rotation.getRight().getDirection());
		Coordinates coordinates = robotPosition.getCoordinates();
		Integer x = coordinates.getX();
		Integer y = coordinates.getY();
		log.info(String.format("Rotate [x=%1s,y=%2s] with direction %3s", x,y, direction));

	}
	
}
