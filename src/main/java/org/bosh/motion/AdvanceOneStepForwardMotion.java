package org.bosh.motion;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bosh.model.AxisDirection;
import org.bosh.model.Coordinates;
import org.bosh.model.RobotPosition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *  Advance {@link RobotPosition} based on its 
 *  {@link AxisDirection} with constant ratio 
 *
 */
@Component("A")
public class AdvanceOneStepForwardMotion implements MotionCommand {

	private final Log log = LogFactory.getLog(AdvanceOneStepForwardMotion.class);
	
	@Autowired
	private MotionUtils utils;
	
	public final void doMove(RobotPosition robotPosition) {
		
		AxisDirection direction = robotPosition.getAxisDirection();
		Coordinates motionCoordinate = utils.getDirectionCoordinates(direction);
		
		Coordinates robotPositionCoordinates = robotPosition.getCoordinates();

		Integer x1 = robotPositionCoordinates.getX();
		Integer y1 = robotPositionCoordinates.getY();

		Integer x2 = motionCoordinate.getX();
		Integer y2 = motionCoordinate.getY();
		
		Integer dx = x1 + x2;
		Integer dy = y1 + y2;
		
		robotPositionCoordinates.setX(dx);
		robotPositionCoordinates.setY(dy);

		log.info(String.format("Advance [x=%1s,y=%2s] with direction %3s to [dx=%4s,dy=%5s]", x1,y1, direction, dx,dy));

	}
	
	@Override
	public String getMotionId() {
		return "advance_one_step_forward_motion";
	}
}
