package org.bosh.motion;

import org.bosh.model.RobotPosition;

/**
 *  Represent motion command.
 *
 * @see https://en.wikipedia.org/wiki/Transformation_matrix
 */
public interface MotionCommand {

	/**
	 * 
	 * @param position
	 */
	void doMove(RobotPosition position);
	
	/**
	 * Command Id
	 * @return
	 */
	String getMotionId();
}
