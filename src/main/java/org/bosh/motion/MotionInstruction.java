package org.bosh.motion;

import org.bosh.service.MotionServiceException;

/**
 * Possible instructions
 *
 */
public enum MotionInstruction {
 
	LEFT('L'),
	RIGHT('R'),
	ADVANCE('A');
	
	public final Character c;
	public final String  cc;
	
	MotionInstruction(Character c){
		this.c = c;
		cc = c.toString();
	}
	
	public static MotionInstruction fromCharacter(char instructionChar){
		switch (instructionChar) {
		case 'L': return MotionInstruction.LEFT;
		case 'R': return MotionInstruction.RIGHT;
		case 'A': return MotionInstruction.ADVANCE;
		default:
			throw new MotionServiceException("Invalid Motion Instruction ["+instructionChar+"]");
		}
	}

	public String getMotionInstructionName() {
		return c.toString();
	}
}
