package org.bosh.motion;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bosh.model.AxisDirection;
import org.bosh.model.Coordinates;
import org.bosh.model.Rotation;
import org.bosh.service.MotionServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

/**
 * 
 */
@Component
public class MotionUtils {

	private final Map<AxisDirection, Rotation> rotationMap = new HashMap<AxisDirection, Rotation>();
	private final Map<AxisDirection, Coordinates> directionVectors = new HashMap<AxisDirection, Coordinates>();
	
	private final Log log = LogFactory.getLog(MotionUtils.class);
	
	@Autowired
	private ApplicationContext context;
	
	/**
	 * Resolve {@link MotionCommand} implementation for {@link MotionInstruction} parameter
	 * 
	 * @param motionInstruction
	 * @return
	 */
	public MotionCommand commandForInstruction(MotionInstruction motionInstruction) {
		String motionInstructionName = motionInstruction.getMotionInstructionName();
		if (!context.containsBean(motionInstructionName)) {
			String errorMessage = String.format("Invalid instruction %s. Stopping.",motionInstruction); 
			log.error(errorMessage);
			throw new MotionServiceException(errorMessage);
		}
		MotionCommand command = MotionCommand.class.cast(context.getBean(motionInstructionName));
		log.info(String.format("Motion instruction %1s was bind to bean %2s", motionInstruction, command));
		return command;
	}

	/**
	 * Initalise 
	 */
	@PostConstruct
	public void init() {
		Rotation north = new Rotation(AxisDirection.NORTH);
		Rotation west = new Rotation(AxisDirection.WEST);
		Rotation south = new Rotation(AxisDirection.SOUTH);
		Rotation east = new Rotation(AxisDirection.EAST);
		
		log.info("Initalize possible rotaions");
		
		north.setLeft(west).setRight(east);
		west.setLeft(south).setRight(north);
		south.setLeft(east).setRight(west);
		east.setLeft(north).setRight(south);

		rotationMap.put(north.getDirection(), north);
		rotationMap.put(south.getDirection(), south);
		rotationMap.put(east.getDirection(), east);
		rotationMap.put(west.getDirection(), west);
		log.info(String.format("Initalize possible rotaions ", rotationMap));

		directionVectors.put(west.getDirection(), new Coordinates(-1, 0));
		directionVectors.put(north.getDirection(), new Coordinates(0, 1));
		directionVectors.put(east.getDirection(), new Coordinates(1, 0));
		directionVectors.put(south.getDirection(), new Coordinates(0, -1));
		log.info(String.format("Initalize possible transititions for directions ", directionVectors));
	}

	public Coordinates getDirectionCoordinates(AxisDirection direction) {
		return directionVectors.get(direction);
	}

	public Rotation getRotationDirection(AxisDirection direction) {
		return rotationMap.get(direction);
	}
}
