package org.bosh.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bosh.model.RobotPosition;
import org.bosh.motion.MotionCommand;
import org.bosh.motion.MotionUtils;
import org.bosh.motion.MotionInstruction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Motion Service Implementation 
 * 
 */
@Service
public class MotionService {

	private final Log log = LogFactory.getLog(MotionService.class);
	
	@Autowired
	private MotionUtils motionUtils;
	
	public RobotPosition trace(RobotPosition robotPosition, MotionInstruction [] instructions) throws MotionServiceException{
		log.info(String.format("Starting transition from robot position %1s. Total instuctions count %2s", robotPosition, instructions.length));
		MotionUtils motionUtils = getMotionUtils();
		
		for (int i = 0; i < instructions.length; i++) {
			MotionInstruction motionInstruction = instructions[i];
			
			
			MotionCommand motionCommand = motionUtils.commandForInstruction(motionInstruction);
			log.info(String.format("Motion sequence: %1s, instruction: %2s, id: %3s started at robot position: %4s", i, motionInstruction, motionCommand.getMotionId(), robotPosition));
			
			motionCommand.doMove(robotPosition);
			
			log.info(String.format("Motion sequence: %1s, instruction: %2s, id: %3s finished. New robot position: %4s", i, motionInstruction,  motionCommand.getMotionId(), robotPosition));
		}
		log.info(String.format("Instuctions finished. Final robot position %s", robotPosition));
		return robotPosition;
	}


	public MotionUtils getMotionUtils() {
		return motionUtils;
	}

}
