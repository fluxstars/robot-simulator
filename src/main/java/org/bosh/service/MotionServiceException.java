package org.bosh.service;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class MotionServiceException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3404395281295519192L;

	public MotionServiceException(String exception) {
		super("Motion Exception : " + exception );
	}
	
}
