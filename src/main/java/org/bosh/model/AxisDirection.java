package org.bosh.model;

import org.bosh.service.MotionServiceException;

/**
 * 
 */
public enum AxisDirection {

	NORTH('N'),
	SOUTH('S'),
	WEST('W'),
	EAST('E');
	
	Character c;
	
	AxisDirection(Character c){
		this.c = c;
	}
	
	public static AxisDirection fromCharacter(char axisDirection){
		switch (axisDirection) {
		case 'N': return AxisDirection.NORTH;
		case 'S': return AxisDirection.SOUTH;
		case 'W': return AxisDirection.WEST;
		case 'E': return AxisDirection.EAST;
		default:
			throw new MotionServiceException("Invalid Axis Direction ["+axisDirection+"]");
		}
	}
	
}
