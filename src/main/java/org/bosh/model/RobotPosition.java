package org.bosh.model;

import java.io.Serializable;

/**
 * Represent robot position and direction
 * in cartesian system
 *
 */
public class RobotPosition implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3033050343172450770L;
	
	private Coordinates coordinates;
	private AxisDirection axisDirection;
	
	public RobotPosition(Coordinates coordinates, AxisDirection axisDirection) {
		this.coordinates = coordinates;
		this.axisDirection = axisDirection;
	}

	public Coordinates getCoordinates() {
		return coordinates;
	}
	
	public AxisDirection getAxisDirection() {
		return axisDirection;
	}
	
	public void setAxisDirection(AxisDirection axisDirection) {
		this.axisDirection = axisDirection;
	}
	
	@Override
	public String toString() {
		return String.format("RobotPosition : Axis Direction %s, Coordinates : %s", axisDirection, coordinates);
	}
}
