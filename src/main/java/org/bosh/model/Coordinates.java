package org.bosh.model;

/**
 * Cartesian coordinates
 */
public class Coordinates {

	private Integer x;
	private Integer y;

	public Coordinates(Integer x, Integer y) {
		this.x = x;
		this.y = y;
	}

	public Integer getX() {
		return x;
	}

	public Integer getY() {
		return y;
	}

	public Coordinates setX(Integer x) {
		this.x = x;
		return this;
	}

	public Coordinates setY(Integer y) {
		this.y = y;
		return this;
	}

	@Override
	public String toString() {
		return String.format("(%s,%s)", x, y);
	}
	
}
