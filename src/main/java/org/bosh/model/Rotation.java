package org.bosh.model;

/**
 * Model of possible axis rotations
 *
 */
public class Rotation {

	final AxisDirection direction;
	Rotation left;
	Rotation right;

	public Rotation(AxisDirection direction) {
		super();
		this.direction = direction;
	}

	public AxisDirection getDirection() {
		return direction;
	}

	public Rotation getLeft() {
		return left;
	}

	public Rotation setLeft(Rotation left) {
		this.left = left;
		return this;
	}

	public Rotation getRight() {
		return right;
	}

	public Rotation setRight(Rotation right) {
		this.right = right;
		return this;
	}

}
