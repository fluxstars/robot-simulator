package org.bosh;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

//import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

//@EnableConfigServer
@SpringBootApplication
public class RobotSimulatorApplication {

//	@Autowired
//    private YAMLConfig myConfig;
 
    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(RobotSimulatorApplication.class);
        app.run();
    }
 
    public void run(String... args) throws Exception {
    	SpringApplication.run(RobotSimulatorApplication.class, args);
//        System.out.println("using environment: " + myConfig.getEnvironment());
//        System.out.println("name: " + myConfig.getName());
//        System.out.println("servers: " + myConfig.getServers());
    }
	
}
