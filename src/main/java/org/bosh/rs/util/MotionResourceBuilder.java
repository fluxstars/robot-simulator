package org.bosh.rs.util;

import org.bosh.model.AxisDirection;
import org.bosh.model.Coordinates;
import org.bosh.model.RobotPosition;
import org.bosh.motion.MotionInstruction;
import org.springframework.stereotype.Component;

@Component
public class MotionResourceBuilder {

	public MotionInstruction[] buildInstructionsFromPath(String path) {
		char [] instructionsChars = path.toUpperCase().toCharArray();
		MotionInstruction [] instructions = new MotionInstruction[instructionsChars.length];
		for (int i = 0; i < instructionsChars.length; i++) {
			instructions[i] = MotionInstruction.fromCharacter(instructionsChars[i]);
		}
		return instructions;
	}

	public RobotPosition buildRobotPosition(Integer x, Integer y, Character direction) {
		Coordinates coordinates = new Coordinates(x, y);
		AxisDirection axisDirection = AxisDirection.fromCharacter(direction);
		RobotPosition robotPosition = new RobotPosition(coordinates, axisDirection);
		return robotPosition;
	}

}
