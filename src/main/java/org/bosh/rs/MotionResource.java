package org.bosh.rs;

import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bosh.model.RobotPosition;
import org.bosh.motion.MotionInstruction;
import org.bosh.rs.util.MotionResourceBuilder;
import org.bosh.service.MotionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST Service for {@link MotionService}
 */
@RestController
public class MotionResource {

	@Autowired
	MotionService service;
	
	@Autowired	
	MotionResourceBuilder resourceBuilder;
	
	@Autowired
	HttpSession session;
	
	private final Log log = LogFactory.getLog(MotionResource.class);
	
	@GetMapping("/trace")
	public RobotPosition trace(@RequestParam(value = "x", defaultValue = "0") Integer x, 
					   @RequestParam(value = "y", defaultValue = "0") Integer y,
					   @RequestParam(value = "direction", defaultValue = "N") Character direction,
					   @RequestParam(value = "path", defaultValue = "", required=true) String path,
					   @RequestParam(value = "audit", defaultValue = "false") Boolean audit) {
		MotionInstruction [] instructions = resourceBuilder.buildInstructionsFromPath(path);
		RobotPosition robotPosition = resourceBuilder.buildRobotPosition(x,y, direction);

		log.info(String.format("Robot motion test received. Session id %1s.", session.getId()));
		robotPosition = service.trace(robotPosition, instructions);
		
		log.info(String.format("Robot motion test for session id %1s complete.", session.getId()));
		return robotPosition;
	}

	
	
}
