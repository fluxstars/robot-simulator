package org.bosh.motion;

import java.io.Serializable;

import org.bosh.model.RobotPosition;

public class MotionInstructionRobotPosition implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5429868843357224773L;
	private MotionInstruction instruction;
	private RobotPosition position;
	public MotionInstruction getInstruction() {
		return instruction;
	}
	public void setInstruction(MotionInstruction instruction) {
		this.instruction = instruction;
	}
	public RobotPosition getPosition() {
		return position;
	}
	public void setPosition(RobotPosition position) {
		this.position = position;
	}
	
	
}
