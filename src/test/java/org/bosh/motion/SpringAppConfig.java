package org.bosh.motion;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"org.bosh.model","org.bosh.motion","org.bosh.rs.util","org.bosh.service"})
public class SpringAppConfig {

}
