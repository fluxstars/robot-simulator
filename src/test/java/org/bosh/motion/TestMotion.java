package org.bosh.motion;

import java.util.ArrayList;

import org.bosh.model.AxisDirection;
import org.bosh.model.RobotPosition;
import org.bosh.rs.util.MotionResourceBuilder;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


/**
 *      { N, 7, 3 }
 *  R – { E, 7, 3 }
 *  A – { E, 8, 3 }
 *  A – { E, 9, 3 }
 *  L – { N, 9, 3 }
 *  A – { N, 9, 4 }
 *  L - { W, 9, 4 }
 *  
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {SpringAppConfig.class})
public class TestMotion {

	@Autowired
	MotionResourceBuilder resourceBuilder;
	
	@Autowired
	MotionUtils motionUtils;
	
	@Test
	public void testScenarioOne() {
		Integer x=7;
		Integer y=3;
		Character direction= 'N';
		String path= "RAALAL";
		
		ArrayList<MotionInstructionRobotPosition> testingPositions = new ArrayList<MotionInstructionRobotPosition>();
		prepareExpectedTestStep(testingPositions, MotionInstruction.RIGHT, AxisDirection.EAST, 7, 3);
		prepareExpectedTestStep(testingPositions, MotionInstruction.ADVANCE, AxisDirection.EAST, 8, 3);
		prepareExpectedTestStep(testingPositions, MotionInstruction.ADVANCE, AxisDirection.EAST, 9, 3);
		prepareExpectedTestStep(testingPositions, MotionInstruction.LEFT, AxisDirection.NORTH, 9, 3);
		prepareExpectedTestStep(testingPositions, MotionInstruction.ADVANCE, AxisDirection.NORTH, 9, 4);
		prepareExpectedTestStep(testingPositions, MotionInstruction.LEFT, AxisDirection.WEST, 9, 4);
		
		MotionInstruction instructions [] = resourceBuilder.buildInstructionsFromPath(path);
		RobotPosition position = resourceBuilder.buildRobotPosition(x, y, direction);
		for (int i = 0; i < instructions.length; i++) {
			MotionInstruction instruction = instructions[i];
			MotionCommand command = motionUtils.commandForInstruction(instruction);
			MotionInstructionRobotPosition expected = testingPositions.get(i);
			command.doMove(position);
			assertRobotPosition(expected, position, instruction);
		}
		
	}
	
	public void prepareExpectedTestStep(ArrayList<MotionInstructionRobotPosition> testingPositions, MotionInstruction instruction, AxisDirection direction, int x, int y) {
		MotionInstructionRobotPosition instructionRobotPosition = new MotionInstructionRobotPosition();
		instructionRobotPosition.setInstruction(instruction);
		RobotPosition position = resourceBuilder.buildRobotPosition(x, y, direction.name().toCharArray()[0]);
		instructionRobotPosition.setPosition(position);
		testingPositions.add(instructionRobotPosition);
	}

	public static void assertRobotPosition(MotionInstructionRobotPosition expected, RobotPosition position, MotionInstruction instruction) {
		Integer expectedX = expected.getPosition().getCoordinates().getX(); 
		Integer expectedY = expected.getPosition().getCoordinates().getY(); 
		AxisDirection expectedDirection = expected.getPosition().getAxisDirection();
		MotionInstruction expectedInstruction = expected.getInstruction();
		
		Assert.assertEquals(expectedX.longValue(), position.getCoordinates().getX().longValue());
		Assert.assertEquals(expectedY.longValue(), position.getCoordinates().getY().longValue());
		Assert.assertEquals(expectedDirection, position.getAxisDirection());
		Assert.assertEquals(expectedInstruction, instruction);
	}
}
