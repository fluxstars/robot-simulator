Motion Instruction mapping to Motion Command URL parameters:

path = 			R | L | A
				R => Right rotation of robot face for angle of 90 degrees
						N -> W -> S -> E
				L => Left rotation of robot face for angle of 90 degrees
						N -> E -> S -> W 
				A => Advance based on direction of robot. Possible reflections 
						(0,1) move up Y axis
						(1,0) move up X axis
						(0,-1) move down Y axis
						(-1,0) move down X axis
x 				= any valid integer [-....0....+]
y 				= any valid integer [-....0....+]
direction =  	N | E | S | W 
				direction towards which robot is faced
Run project:

mvn clean package
mvn exec:java
curl -X GET -H "Content-type: application/json" -H "Accept: application/json" "http://localhost:8080/trace?x=7&y=3&direction=N&path=RAALAL"
