#!/bin/bash
FROM openjdk:8-jdk-alpine
# Add Maintainer Info
LABEL maintainer="support@flux-stars.ch"
# Make port 8080 available to the world outside this container
EXPOSE 8080
VOLUME /tmp
ARG JAR_FILE
#COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/robot-simulator.jar"]